Cards

-------------------------------------------------------------------------------

project         = "Card"
author          = "Michael Sch�fers",
contact         = "schafers@informatik.haw-hamburg.de",
organization    = "Dept.Informatik; HAW Hamburg",
date            = "2012/11/19",
version         = "2.1",
note            = "release for WS12/13 ;  1st release WS07/08",
lastModified    = "2013/01/14",
lastModifiedBy  = "Michael Sch�fers",